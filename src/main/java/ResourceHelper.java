import com.google.common.io.Resources;

import java.io.InputStream;
import java.net.URL;

public class ResourceHelper {

    public static InputStream openInputStream(String resourcePath) {
        try {
            URL url = Resources.getResource(resourcePath);
            return url.openStream();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
