import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.spi.json.JacksonJsonNodeJsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import com.opencsv.CSVReader;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

public class Logic {
    private final static Logger logger = LogManager.getLogger(Logic.class);

    private static final String AUTH_TOKEN = "Bearer b0769ca9-aaad-404f-a94f-d7e9adcba72c";
    private static final String FILE_WITH_TENANTS = "Untitled spreadsheet - Sheet1.csv";
    private final String FILENAME = "./jobs.txt";
    private final String SYNC_REQUEST_BODY = readRequest();

    public static String readResource(Class clazz, String name) throws IOException {
        InputStream stream = clazz.getClassLoader().getResourceAsStream(name);
        return org.apache.commons.io.IOUtils.toString(stream, StandardCharsets.UTF_8);
    }

    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();

    {
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", AUTH_TOKEN);
    }

    static Map<String, String> envUrlsAf = new LinkedHashMap<>();

    static {
        envUrlsAf.put("sndbx", "https://reltio-af-sndbx.reltio.com");
        envUrlsAf.put("pilot", "https://reltio-af-pilot.reltio.com");
        envUrlsAf.put("test", "https://reltio-af-test.reltio.com");
        envUrlsAf.put("geu-test", "https://geu-test-stage-af.reltio.com");
        envUrlsAf.put("dev", "https://reltio-af-dev.reltio.com");
        envUrlsAf.put("prod-usg", "https://prod-usg-af.reltio.com");
        envUrlsAf.put("test-usg", "https://test-usg-af.reltio.com");
        envUrlsAf.put("gus-sales", "http://gus-sales-af.reltio.com");
        envUrlsAf.put("gus-training", "http://gus-training-af01.gus-training.reltio.com");
        envUrlsAf.put("361", "https://reltio-af-361.reltio.com");
        envUrlsAf.put("eu-360", "https://euprod-01-af.reltio.com");
        envUrlsAf.put("eu-dev", "https://eudev-01-af.reltio.com");
        envUrlsAf.put("mpe-01", "https://eudev-01-af.reltio.com");
        envUrlsAf.put("mpe-02", "https://mpe-02-af.reltio.com");
        envUrlsAf.put("mpe-03", "https://360p1-af.reltio.com");
        envUrlsAf.put("gca-test", "http://gca-test-af01.gca-test.reltio.com");
        envUrlsAf.put("ap-360", "https://approd-01-af.reltio.com");

        envUrlsAf.put("prod-h360", "http://prod-h360-af.reltio.com");
        envUrlsAf.put("test-h360", "http://test-h360-af.reltio.com");
        envUrlsAf.put("dev-h360", "http://dev-h360-af.reltio.com");
    }

    static Map<String, String> envUrlsExport = new LinkedHashMap<>();

    static {
        envUrlsExport.put("sndbx", "https://sndbx.reltio.com");
        envUrlsExport.put("pilot", "https://pilot.reltio.com");
        envUrlsExport.put("test", "https://test.reltio.com");
        envUrlsExport.put("geu-test", "https://geu-test.reltio.com");
        envUrlsExport.put("dev", "https://dev.reltio.com");
        envUrlsExport.put("prod-usg", "https://prod-usg.reltio.com");
        envUrlsExport.put("test-usg", "http://test-usg.reltio.com");
        envUrlsExport.put("gus-sales", "https://gus-sales.reltio.com");
        envUrlsExport.put("gus-training", "https://gus-training.reltio.com");
        envUrlsExport.put("361", "https://360.reltio.com");
        envUrlsExport.put("eu-360", "https://eu-360.reltio.com");
        envUrlsExport.put("mpe-01", "https://eu-dev.reltio.com");
        envUrlsExport.put("eu-dev", "https://eu-dev.reltio.com");
        envUrlsExport.put("mpe-02", "https://ap-dev.reltio.com");
        envUrlsExport.put("mpe-03", "https://mpe-03.reltio.com");
        envUrlsExport.put("gca-test", "https://gca-test.reltio.com");

        envUrlsAf.put("ap-360", "https://ap-360.reltio.com");
        envUrlsAf.put("prod-h360", "https://prod-h360.reltio.com");
        envUrlsAf.put("test-h360", "https://test-h360.reltio.com");
        envUrlsAf.put("dev-h360", "https://dev-h360.reltio.com");
    }


    static class SyncJobInfo {
        final String tenant;
        final String jobUrl;
        boolean completed;
        String syncJobUrl;
        boolean switchedToV2;

        SyncJobInfo(String tenant, String jobUrl, boolean completed, String syncJobUrl, boolean switchedToV2) {
            this.tenant = tenant;
            this.jobUrl = jobUrl;
            this.completed = completed;
            this.syncJobUrl = syncJobUrl;
            this.switchedToV2 = switchedToV2;
        }
    }


    Map<String, Map<String, SyncJobInfo>> runningJobsOnEnvs = new LinkedHashMap<>();

    public void action() throws IOException {

        System.err.println("Started ");
        File tmpDir = new File(FILENAME);
        boolean exists = tmpDir.exists();
        if (!exists) {
            FileWriter myWriter = new FileWriter(FILENAME);
            myWriter.write("");
            myWriter.close();
        } else {
            try (BufferedReader br = new BufferedReader(new FileReader(tmpDir))) {
                for (String line; (line = br.readLine()) != null; ) {
                    // process the line.
                    String[] split = line.split(",");
                    String env = split[0].trim();
                    boolean switchedToV2 = split.length > 4 ? Boolean.parseBoolean(split[4].trim()) : false;
                    SyncJobInfo syncJobInfo = new SyncJobInfo(split[1].trim(), split[2].trim(), Boolean.parseBoolean(split[3].trim()), null, switchedToV2);
                    runningJobsOnEnvs.computeIfAbsent(env, s -> new LinkedHashMap<>()).put(syncJobInfo.tenant, syncJobInfo);
                }
                // line is not visible here.
            }
        }

        checkStatusAndRemoveCompleted(runningJobsOnEnvs);
//        try (BufferedWriter bw = new BufferedWriter(new FileWriter("./completed.txt", true))) {
//            Set<Map.Entry<String, String>> entries = completed.entrySet();
//            for (Map.Entry<String, String> entry : entries) {
//                String tenant = entry.getKey();
//                String url = entry.getValue();
//                bw.write(tenant + ", " + url);
//                bw.newLine();
//            }
//        }
//
//


        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(FILE_WITH_TENANTS);
        StringReader stringReader = new StringReader(IOUtils.toString(inputStream, StandardCharsets.UTF_8));

        try {
            try (CSVReader csvReader = new CSVReader(stringReader)) {

                Iterator<String[]> iterator = csvReader.iterator();
                String[] headers = iterator.next();

                while (iterator.hasNext()) {
                    String[] line = iterator.next();

                    String env = line[0];
                    String tenant = line[1];
                    //Uncomment AF enablement for first round of migration phase.
                    /*
                    try {
                        enableAF(tenant,env);
                        System.err.println("AF Enabled for "+ tenant + "@" + env);

                    } catch (Exception e) {
                      System.err.println("AF not enabled for " + tenant + "@" + env);
                      continue;
                    }

                    if(true){
                        continue;
                    }
                    */

                    Map<String, SyncJobInfo> jobsOnEnv = runningJobsOnEnvs.computeIfAbsent(env, s -> new LinkedHashMap<>());
                    if (jobsOnEnv.values().stream().filter(syncJobInfo -> !syncJobInfo.completed).count() >= 10) {
                        //skip for env
                        System.err.println("Env " + env + " has >10 pending jobs");
                        continue;
                    }
                    if (jobsOnEnv.get(tenant) != null && jobsOnEnv.get(tenant).completed) {
                        SyncJobInfo jobInfo = jobsOnEnv.get(tenant);

                        if (jobInfo != null && jobInfo.switchedToV2 == false) {
                            boolean switchedToV2 = updatePostProcessingFlagAndSwitchToV2(tenant, env);
                            if (switchedToV2) {
                                jobInfo.switchedToV2 = true;
                            }
                        }
                        continue;
                    } else if (jobsOnEnv.get(tenant) == null) {
                        String jobUrl = runSyncTask(tenant, env);

                        if (jobUrl != null) {
                            System.err.println("Sync job " + jobUrl + " for tenant " + tenant);
                            jobsOnEnv.put(tenant, new SyncJobInfo(tenant, jobUrl, false, null, false));
                        } else {
                            System.err.println("Sync job not started: " + tenant + " " + env);
                        }
                    }
                }
            }
        } finally {
            FileWriter myWriter = new FileWriter(FILENAME);
            AtomicLong completed = new AtomicLong();
            AtomicLong pending = new AtomicLong();
            for (Map.Entry<String, Map<String, SyncJobInfo>> es : runningJobsOnEnvs.entrySet()) {
                String env = es.getKey();
                Map<String, SyncJobInfo> jobs = es.getValue();
                jobs.forEach((tenant, jobInfo) -> {
                    try {
                        String str = env + ", " + tenant + ", " + jobInfo.jobUrl + " , " + jobInfo.completed + ", " + jobInfo.switchedToV2 + " \n";
                        myWriter.write(str);
//                        System.out.print(str);
                        if (jobInfo.completed) {
                            completed.incrementAndGet();
                        } else {
                            pending.incrementAndGet();
                        }
                    } catch (IOException e) {
                        System.err.println(e.getMessage());
                    }
                });
            }
            System.err.println("Summary: completed=" + completed + " pending=" + pending);

            myWriter.close();
        }
    }

    private String runSyncJob(String tenant, String env) {
        String baseUrl = envUrlsAf.get(env);
        if (baseUrl == null) {
            System.err.println("URL not found for " + tenant + " " + env);
            return null;
        }
        String url = baseUrl + "/api/v1.0/jobs/";

        String response;
        try {
            String body = SYNC_REQUEST_BODY.replace("{{tenant}}", tenant);
            response = doPost(url, body);
        } catch (Exception e) {
            System.err.println("tenant " + tenant + " failed to sync: " + e.getMessage());
            return null;
        }

        Map<String, String> result;
        try {
            result = new ObjectMapper().readValue(response, HashMap.class);
        } catch (JsonProcessingException e) {
            System.err.println("Parse failed " + tenant + " " + e.getMessage());
            return null;
        }
        return url + result.get("id");
    }

    static final Configuration configuration = Configuration.builder()
            .jsonProvider(new JacksonJsonNodeJsonProvider())
            .mappingProvider(new JacksonMappingProvider())
            .build();

    private boolean updatePostProcessingFlagAndSwitchToV2(String tenant, String env) {
        String baseUrl = envUrlsExport.get(env);
        if (baseUrl == null) {
            return false;
        }
        return updatePostProcessingFlag2(baseUrl, tenant, env);
    }

    private boolean updatePostProcessingFlag2(String baseUrl, String tenant, String env) {

        try {
            String response = doGet(baseUrl + "/reltio/tenants/" + tenant);

            DocumentContext parse = JsonPath.using(configuration)
                    .parse(response);
            Boolean skipPPPrevious;
            try {
                skipPPPrevious = parse.read("$.exportConfig.skipPostprocessing", Boolean.class);
            }catch (Exception e){
                skipPPPrevious = null;
            }

            JsonNode updatedJson = parse
                    .put("$.exportConfig", "skipPostprocessing", skipPPPrevious != null ? skipPPPrevious : false)
                    .set("$.exportConfig.exportVersion", "V2")
//                    .put("$.exportConfig.smartExport","secondaryDsEnabled", false)
                    .json();


            String updatedConfig = updatedJson.toString();
            String resp = doPost(baseUrl + "/reltio/tenants/?tenantId=" + tenant, updatedConfig);
//            if (!updatedConfig.equals(resp)) {
//                System.err.println("Diff: ");
//            }

            System.err.println("V2 Enabled for tenant: " + tenant);
            return true;
        } catch (Exception e) {
            System.err.println("V2 not enabled for tenant: " + tenant + " " + e.getMessage());
        }

        return false;
    }

    private boolean switchToV2(String tenant, String env) {
        String baseUrl = envUrlsExport.get(env);
        if (baseUrl == null) {
            return false;
        }

        String url = baseUrl + "/jobs/settings/" + tenant + "/exportVersion";
        try {
            String response = doPost(url, "{\n" +
                    "    \"value\": \"v2\"\n" +
                    "}");
            System.err.println("V2 Enabled for tenant: " + tenant);
        } catch (Exception e) {
            System.err.println("V2 not enabled for tenant: " + tenant + " " + e.getMessage());
        }
        return true;
    }

    private void checkStatusAndRemoveCompleted(Map<String, Map<String, SyncJobInfo>> runningJobsOnEnvs) {
        for (Map.Entry<String, Map<String, SyncJobInfo>> es : runningJobsOnEnvs.entrySet()) {
            Map<String, SyncJobInfo> jobs = es.getValue();

            List<String> toRemove = new ArrayList<>();
            for (Map.Entry<String, SyncJobInfo> es2 : jobs.entrySet()) {
                String key = es2.getKey();
                SyncJobInfo jobInfo = es2.getValue();
                if (jobInfo.completed) {
                    continue;
                }
                String response;
                try {
                    response = doGet(jobInfo.jobUrl);
                } catch (Exception e) {
                    System.err.println("Failed status check " + jobInfo.jobUrl + " " + e.getMessage());
                    continue;
                }
                try {
                    HashMap hashMap = new ObjectMapper().readValue(response, HashMap.class);
                    Object status = hashMap.get("status");
                    if (status.equals("COMPLETED")) {
                        System.err.println("Job completed " + jobInfo.jobUrl + " for tenant " + jobInfo.tenant);
                        jobInfo.completed = true;
                    } else if (status.equals("PROCESSING")) {
                        System.err.println("Job processing " + jobInfo.jobUrl + " for tenant " + jobInfo.tenant);
                    } else if (status.equals("JOB_CANCELED")) {
                        System.err.println("Job " + status + " " + jobInfo.jobUrl + " for tenant " + jobInfo.tenant);
                        toRemove.add(key);
                    } else {
                        System.err.println("Job " + status + " " + jobInfo.jobUrl + " for tenant " + jobInfo.tenant);
                    }
                } catch (JsonProcessingException e) {
                    System.err.println("Status check failed: " + jobInfo.jobUrl + " " + e.getMessage());
                }
            }
            toRemove.forEach(jobs::remove);
        }

    }

    private void enableAF(String tenant, String env) {
        String baseUrl = envUrlsAf.get(env);
        if (baseUrl == null) {
            return;
        }
        String url = baseUrl + "/api/v1.0/configuration/" + tenant;
        try {
            doPut(url, "{\n" +
                    "    \"analyticsEnabled\": true\n" +
                    "}");
            logger.info("AF Enabled for tenant: " + tenant);
        } catch (Exception e) {
            System.err.println("AF not enabled for tenant: " + tenant + " " + e.getMessage());
        }
    }

    private String runSyncTask(String tenant, String env) {
        String baseUrl = envUrlsAf.get(env);
        if (baseUrl == null) {
            System.err.println("URL not found for " + tenant + " " + env);
            return null;
        }
        String url = baseUrl + "/api/v1.0/jobs/";

        String response;
        try {
            String body = SYNC_REQUEST_BODY.replace("{{tenant}}", tenant);
            response = doPost(url, body);
        } catch (Exception e) {
            System.err.println("tenant " + tenant + " failed to sync: " + e.getMessage());
            return null;
        }

        Map<String, String> result;
        try {
            result = new ObjectMapper().readValue(response, HashMap.class);
        } catch (JsonProcessingException e) {
            System.err.println("Parse failed " + tenant + " " + e.getMessage());
            return null;
        }
        return url + result.get("id");
    }

    public String readRequest() {
        File file = new File("./request.json");
        try {
            return FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    String doGet(String url) {
        return doPutOrSend(url, null, HttpMethod.GET);
    }

    String doPut(String url, String body) {
        return doPutOrSend(url, body, HttpMethod.PUT);
    }

    String doPost(String url, String body) {
        return doPutOrSend(url, body, HttpMethod.POST);
    }

    private String doPutOrSend(String url, String body, HttpMethod post) {
        HttpEntity<String> getRequest = new HttpEntity<>(body, headers);
        ResponseEntity<String> exchange;
        try {
            exchange = restTemplate.exchange(url, post, getRequest, String.class);
        } catch (HttpClientErrorException e) {
            throw e;
        }
        if (exchange.getStatusCode().is2xxSuccessful()) {
            return exchange.getBody();
        } else {
            throw new HttpClientErrorException(exchange.getStatusCode());
        }

    }

    public static void main(String[] args) {
        new Logic().updatePostProcessingFlag2("http://localhost:8080", "Merill", "lc");
    }
}
